#include <stdio.h>

int rng(){
	FILE *fp;				// file pointer 
	char seed;	
	int i;		
	fp = fopen("/dev/urandom", "r");	// filepointer = open file /dev/urandom RO
	fread(&seed, 1, 1, fp);			// read first byte from /dev/urandom into $seed
	fclose(fp);				// close *FP
	srand(seed);				// seed rng with $seed
	i = rand();
	return i;
}

int scramble(){
	char movements[18][4] = { "F", "R", "U", "L", "B", "D", "F'", "R'", "U'", "L'", "B'", "D'", "F2", "R2", "U2", "L2", "B2", "D2" };	
	char sides[6][2] = {"FB", "RL", "UD", "LR", "BF", "DU"};
	int i,j,r,rp,rpp,s;
	rp = NULL;
	rpp = NULL;
	i = 0;
	while(i < 18){
		if(i==0){						// if first loop, just fucking print the movement
			r = rng() % 18;
			for(j = 0; j < 4; j++){
				printw("%c", movements[r][j]);
			}
			printw(" ");					// legibility 
			i = i + 1;
			rp = r;						// set rp for the next block, if this stays NULL it will create duplicate first moves
		} else{							// if !first loop, check for repeat movement
			r = rng() % 18;					


			if(movements[rp][0] == movements[r][0]){	// check if this movement begins same as the last movement
				continue;								
			} else if(rpp != NULL && movements[rpp][0] == movements[r][0]){ 
				continue;				// check if last last = cur OR if last last = opposite
			} else if(rpp != NULL && movements[rpp][0] == sides[s][1]){	
				continue;
			} else{						
				for(j = 0; j < 4; j++){			// if movement !repeat, print it
					printw("%c", movements[r][j]);
				}
				printw(" ");				// for legibility
				i = i + 1;
				rpp = rp;				// set vars for next iteration ONLY IF they passed the checks
				rp = r;						
				s = rpp % 6;
			}
		}
	}

	printw("\n");							// for not making Mr Terminal an ugly clusterfuck at exit time
	return 0;
}
