int tmpwrite(s, n){
	
	FILE *fp;				// file pointer 
	fp = fopen("/tmp/cubetimer", "ab+");	// filepointer = open file /dev/urandom RO
	fprintf(fp, "%lld.%lld \n", s,n);			// read first byte from /dev/urandom into $seed
	fclose(fp);				// close *FP
	return 0;

}


int keypress(void){
	int c = getch();

	if(c == 'q'){
		nocbreak();
		endwin();
		delscreen(stdscr);
		exit(0);
	} else if(c == ' '){
		return 1;
	} else {
		return 0;
	}
}


int ss(){
	while(1){
		if(keypress()){
			cubetimer();
		} else{
			continue;
		}
	}

	return 0;
}

int cubetimer(){
	struct timespec start, cur, delta;
	int s, sd, n, m;
	m = 0;
	clockid_t clk_id;
	clk_id = CLOCK_REALTIME;
	
	clock_gettime(clk_id, &start);
	while(1){
		clock_gettime(clk_id, &cur);
		
		s = difftime(cur.tv_sec, start.tv_sec);
		
		if(s % 60 == 0){
			m = s / 60;
		}
		
		sd = s % 60;
		delta.tv_nsec = cur.tv_nsec + start.tv_nsec;
		n = difftime(delta.tv_nsec, start.tv_nsec);

		//n = abs(n);
		
		printw("%lld:", m);
		
		if(sd<10){
			printw("0");
		}
		printw("%d:", sd);
		printw("%d", n);
		printw("\r");
		refresh();
		if(keypress()){
			printw("%lld:", m);
			if(s<10){
				printw("0");
			}
			printw("%lld:%lld\n", sd,n);


			tmpwrite(s,n);
			statusbar(m, sd, n);
			refresh();
			ss();
		} 
	}
}
