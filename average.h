int avg(ll){
	ll = nlines();

	float a, b, c, d, e, avg3, avg5, avg3dec, avg5dec;
	int avg3trunc, avg5trunc, m3, m5;
	char ns3[20], ns5[20];
	int i = 0;
	int j;
	char buff[256];
	char delim[] = " ";

	FILE *fp;
	fp = fopen("/tmp/cubetimer", "r");

	while(fgets(buff,sizeof(buff), fp)){
		i++;
		j = ll - i;
		if(j <= 4){
			switch(j) {
				case 0:
					a = atof(buff);
					break;
				case 1:
					b = atof(buff);
					break;
				case 2:
					c = atof(buff);
					break;
				case 3:
					d = atof(buff);
					break;
				case 4:
					e = atof(buff);
					break;
			}
		}
	}

	fclose(fp);


	//printf("%0.6f\t %0.6f\t %0.6f\t %0.6f\t %0.6f\t", a, b, c, d, e);
	avg3 = (a+b+c) / 3;
	avg5 = (a+b+c+d+e)/5;

	avg3trunc = truncl(avg3);
	avg5trunc = truncl(avg5);


	//avg3trunc = avg3;
	//avg5trunc = avg5;
	
	avg3dec = avg3 - avg3trunc;
	avg5dec = avg5 - avg5trunc;

	sprintf(ns3, "%f", avg3dec);
	sprintf(ns5, "%f", avg5dec);

	//printw("3Avg: %0.6f\t 5Avg: %0.6f\t", avg3, avg5);

	if(avg3trunc % 60 == 0 || avg3trunc >=60){
		m3 = avg3trunc / 60;
	} else{
		m3 = 0;
	}

	if(avg5trunc % 60 == 0 || avg5trunc >=60){
		m5 = avg5trunc / 60;
	} else{
		m5 = 0;
	}

	avg3trunc = avg3trunc % 60;
	avg5trunc = avg5trunc % 60;


	printw("3avg: ");
	printw("%d:", m3);
	if(avg3trunc<10){
		printw("0");
	}
	printw("%d:%s\t", avg3trunc, ns3+2);

	printw("5avg: ");
	printw("%lld:", m5);
	if(avg5trunc<10){
		printw("0");
	}
	printw("%d:%s\t", avg5trunc, ns5+2);


	refresh();
	return 0;

}

int nlines(){
	int lines = 0;
	char ch;

	FILE *fp;
	fp = fopen("/tmp/cubetimer", "r");
	

	while((ch=fgetc(fp))!=EOF){
			if(ch=='\n')
			lines++;
	}
	fclose(fp);
	return lines;

}


