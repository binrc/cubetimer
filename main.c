#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <curses.h>
#include <math.h>
#include "average.h"
#include "scramble.h"
#include "statusbar.h"
#include "timer.h"


int main(){
	initscr();		// inits screen. implies WINDOW *stdscr;
	cbreak();		// allow 
	noecho();		// don't echo chars from stdin
	timeout(10);		// tt sleep while waiting for imput
	curs_set(0);
	start_color();
	scrollok(stdscr, TRUE); // stdscr is the pointer for the default win

	init_pair(1, COLOR_BLACK, COLOR_MAGENTA);				// init status bar
	attron(COLOR_PAIR(1));
	printw("Press the the space bar to start and stop. Press q to quit.");
	attroff(COLOR_PAIR(1));
	printw("\n");

	init_pair(2, COLOR_MAGENTA, COLOR_BLACK);				// first scramble
	attron(COLOR_PAIR(2));
	scramble();
	attroff(COLOR_PAIR(2));

	refresh();


	// init tmpfile with zeros, messy hack but prevents weird math bugs
	FILE *fp;				
	fp = fopen("/tmp/cubetimer", "ab+");	
	int i;
	for(i=0; i<5; i++){
		fprintf(fp, "0.0 \n");			
	}
	fclose(fp);				

	ss();
	return 0;
}

