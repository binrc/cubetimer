cubetimer: main.c
	cc -o cubetimer main.c -lcurses -lm

debug: 
	cc -g -o cubetimer main.c -lcurses -lm -Wall -Wextra  && gdb cubetimer

run: cubetimer
	./cubetimer

install: 
	mkdir -p /opt && cp ./cubetimer /opt/cubetimer && chmod a+x /opt/cubetimer

clean: 
	rm ./cubetimer 

tarball: 
	touch cubetimer cubetimer.tar.gz && make clean && rm ./cubetimer.tar.gz && tar czfv /tmp/cubetimer.tar.gz ../cubetimer && mv /tmp/cubetimer.tar.gz ./
