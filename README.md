# Works With:
- GNU/Linux (gcc)
- OpenBSD (gcc)
- FreeBSD (llvm)

# Building
### fedora, rhel, centos
```shell
sudo dnf groupinstall "Development Tools"
sudo dnf install ncurses-devel
make run
```

### openbsd, freebsd
```
make run
```
