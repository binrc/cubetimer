statusbar(m, sd, n){
	int y,x;
	getyx(stdscr, y, x);
	move(0,0);
	init_pair(1, COLOR_BLACK, COLOR_MAGENTA);
	attron(COLOR_PAIR(1));
	printw("\r");
	printw("Last: ");
	printw("%lld:", m);
	if(sd<10){
		printw("0");
	}
	printw("%d:%d\t", sd, n);
	avg();
	printw("\n");
	attroff(COLOR_PAIR(1));
	init_pair(2, COLOR_MAGENTA, COLOR_BLACK);
	attron(COLOR_PAIR(2));
	scramble();
	attroff(COLOR_PAIR(2));
	move(y,x);
	return 0;
}
